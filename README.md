
## Petstagram     

### Group members   
Yanying Yu /yanying6   
Tianhao Chen/tc30   
Hao Jia/haoj4   

### Description   
While apps like Tinder, Twitter, and Instagram have made it easier for us to meet new people and expand our social circles, it's only fair that we provide the same opportunities for our pet companions. In light of the success of Instagram, we've been inspired to build a site we're calling Petstagram, specifically for the purpose of assisting pet owners to share their pets and make friends. While this service certainly benefits our furry friends, it also has the potential to aid humans. Pets are a great way for us to bond with and meet new people.  

###  Features  
1. User authentication signup/login/logout  
2. Create a post: user can create a new post by providing the post title, description, address and an image.  
3. Edit /delete a post: user can edit or delete their post.  
4. Comment: User can make comment or delete their comment.  
5. Trending page: see the post trending, allow to sort post by different variables(time, title, comments). 
6. Following/Unfollow user: Users are allowed to following or unfollow other users. 
7. Following page: see the user list of your following.    
8. Post detail: User can see the detailed post if they have already logined. they can see the address by google map.  


### Tech   
Frontend- Express  
Backend-  React   
Database- MongoDB  
Imagefile- AWS S3  

Deployement:    
See the url: https://petstagram-e5e84.web.app/      
Frontend- Firebase   
Backend-  Heroku   


### Reference     
https://stackoverflow.com/questions/8303900/mongodb-mongoose-findmany-find-all-documents-with-ids-listed-in-array    
https://dev.to/salehmubashar/search-bar-in-react-js-545l  
https://ramonak.io/posts/react-how-to-sort-array-of-objects-with-dropdown-and-hooks   
https://cloudinary.com/documentation/react_image_and_video_upload  
https://www.udemy.com/course/react-nodejs-express-mongodb-the-mern-fullstack-guide/   
